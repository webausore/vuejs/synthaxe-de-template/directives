const app = new Vue({
	el: '#app',
	data: {
		hide: false,
		googleSearch: '',
		event: 'mouseenter',
	},
	methods: {
		animation(e) {
			e.target.classList.toggle('animation');
		},
		onChange(e) {
			this.event = e.target.value;
		},
	},
});
