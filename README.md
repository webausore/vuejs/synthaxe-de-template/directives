# Vue JS - Synthaxe de template - Directives

## Exercice

Préview : https://vuejs-synthaxe-de-vue-directives.netlify.app/

Reproduire le [contenu de cette page](https://vuejs-synthaxe-de-vue-directives.netlify.app/) à l'exacte identique ainsi que les comportements sur la page.
